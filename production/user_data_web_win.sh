<script>
:: Script tags always run first
echo "The S3 bucket is ${bootstrap_s3_bucket_name}" > C:\bootstrap.txt
echo "The region is ${region}" >> C:\bootstrap.txt
echo "The server_type is ${server_type}" >> C:\bootstrap.txt
</script>

<powershell>
Set-ExecutionPolicy Unrestricted -Force
New-Item -ItemType directory -Path 'C:\temp'
 
# Install IIS and Web Management Tools.
Import-Module ServerManager
install-windowsfeature web-server, web-webserver -IncludeAllSubFeature
install-windowsfeature web-mgmt-tools
</powershell>
