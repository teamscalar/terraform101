variable "region" { default = "us-east-1" }
variable "instance_type" { default = "t2.micro" }
variable "key_name" { default = "us-east-key" }
variable "vpc_id" { default = "vpc-94bd76ec" }

/* Setup access to AWS */
provider "aws" {
  region     = "${var.region}"
}

module "web_sec_group" {
  source          = "../modules/web-sg"
  vpc_id          = "${var.vpc_id}"
  security_groups = "${module.elb_sec_group.elb_sg_id}"  
}


data "template_file" "web_user_data" {
  template = "${file("user_data_web.sh")}"

  vars {
    region                   = "${var.region}"
    bootstrap_s3_bucket_name = "tf101_YOURNAME_bootstrap"
    server_type              = "web"
  }
}


module "webserver" {
  source        = "../modules/instance"
  server_count  = "1"
  web_sg_id     = "${module.web_sec_group.web_sg_id}" 
  instance_type = "${var.instance_type}"
  server_type   = "prodstg"
  key_name      = "${var.key_name}"
  user_data     = "${data.template_file.web_user_data.rendered}"
}

module "webserver2" {
  source        = "../modules/instance"
  server_count  = "1"
  web_sg_id     = "${module.web_sec_group.web_sg_id}" 
  instance_type = "${var.instance_type}"
  server_type   = "prodweb"
  key_name      = "${var.key_name}"
  user_data     = "${data.template_file.web_user_data.rendered}"
  subnet_id     = "subnet-5fee3514"
}

module "elb_sec_group" {
  source    = "../modules/elb-sg"
}

module "elb_web" {
  source          = "../modules/elb"
  subnets         = ["subnet-49736b65","subnet-5fee3514"]
  security_groups = "${module.elb_sec_group.elb_sg_id}"
  instances       = "${module.webserver.instance_id}"
}
