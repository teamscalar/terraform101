#!/bin/bash
yum update -y
yum install -y httpd24
service httpd start
chkconfig httpd on
echo "The S3 bucket is ${bootstrap_s3_bucket_name}" > /tmp/bootstrap.txt
echo "The region is ${region}" >> /tmp/bootstrap.txt
echo "The server_type is ${server_type}" >> /tmp/bootstrap.txt
