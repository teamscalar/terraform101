/*
variable "vpc_id" { default = "vpc-94bd76ec" }
variable "ingress_from_port" { default = "80" }
variable "ingress_to_port" { default = "80" }
variable "ingress_protocol" { default = "tcp" }
variable "ingress_cidr" { default = "0.0.0.0/0" }
variable "egress_from_port" { default = "0" }
variable "egress_to_port" { default = "0" }
variable "egress_protocol" { default = "-1" }
variable "egress_cidr" { default = "0.0.0.0/0" }
variable "server_type" { default = "prodweb" }
*/

resource "aws_security_group" "elb_YOURNAME_sg" {
  name        = "tf101-${var.server_type}-elb-YOURNAME-sg"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = "${var.ingress_from_port}"
    to_port     = "${var.ingress_to_port}"
    protocol    = "${var.ingress_protocol}"
    cidr_blocks = ["${var.ingress_cidr}"]
  }

  egress {
    from_port   = "${var.egress_from_port}"
    to_port     = "${var.egress_to_port}"
    protocol    = "${var.egress_protocol}"
    cidr_blocks = ["${var.egress_cidr}"]
  }

  tags {
    Name = "tf101-${var.server_type}-elb-YOURNAME-sg"
  }
}
