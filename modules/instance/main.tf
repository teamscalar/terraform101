variable "ami" { default = "ami-8c1be5f6" }
variable "instance_type" { default = "t2.micro" }
variable "subnet_id" { default = "subnet-49736b65" }
variable "web_sg_id" {} 
variable "server_type" { default = "prodweb" }
variable "key_name" { default = "us-east-key" }
variable "server_count" { default = "1" }
variable "user_data" { default = "user_data_web.sh" }

# Create an instance
resource "aws_instance" "instance_YOURNAME" {
  count                       = "${var.server_count}"
  ami                         = "${var.ami}"
  instance_type               = "${var.instance_type}"
  associate_public_ip_address = "true"
  vpc_security_group_ids      = ["${var.web_sg_id}"]
  subnet_id                   = "${var.subnet_id}"
  key_name                    = "${var.key_name}"
  user_data                   = "${var.user_data}"

  tags {
    Name = "tf101-${var.server_type}-instance-YOURNAME"
  }
}
