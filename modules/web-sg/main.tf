resource "aws_security_group" "instance_YOURNAME_sg" {
  name        = "tf101-${var.server_type}-instance-YOURNAME-sg"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = "${var.ingress_from_port}"
    to_port     = "${var.ingress_to_port}"
    protocol    = "${var.ingress_protocol}"
    cidr_blocks = ["${var.ingress_cidr}"]
  }

  ingress {
    from_port       = "${var.ingress_from_port}"
    to_port         = "${var.ingress_to_port}"
    protocol        = "${var.ingress_protocol}"
    security_groups = ["${var.security_groups}"]
  }

  egress {
    from_port   = "${var.egress_from_port}"
    to_port     = "${var.egress_to_port}"
    protocol    = "${var.egress_protocol}"
    cidr_blocks = ["${var.egress_cidr}"]
  }

  lifecycle {
    ignore_changes = ["name"]
  }

  tags {
    Name = "tf101-${var.server_type}-instance-YOURNAME-sg"
  }
}
