output "elb_id" {
  value = "${aws_elb.elb_YOURNAME.id}"
}

output "elb_dns_name" {
  value = "${aws_elb.elb_YOURNAME.dns_name}"
}