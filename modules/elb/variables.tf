variable "subnets" { type = "list" }
variable "security_groups" {}
variable "instances" {}
variable "server_type" { default = "prod" }
variable "elb_instance_port" { default = "80" }
variable "elb_instance_protocol" { default = "http" }
variable "elb_port" { default = "80" }
variable "elb_protocol" { default = "http" }
variable "hc_healthy_threshold" { default = "2" }
variable "hc_unhealthy_threshold" { default = "2" }
variable "hc_timeout" { default = "3" }
variable "hc_target" { default = "TCP:80" }
variable "hc_interval" { default = "30" }
