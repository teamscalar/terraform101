resource "aws_elb" "elb_YOURNAME" {
  name            = "tf101-${var.server_type}-elb-YOURNAME"
  subnets         = ["${var.subnets}"]
  security_groups = ["${var.security_groups}"]
  instances       = ["${var.instances}"]

  listener {
    instance_port     = "${var.elb_instance_port}"
    instance_protocol = "${var.elb_instance_protocol}"
    lb_port           = "${var.elb_port}"
    lb_protocol       = "${var.elb_protocol}"
  }

  health_check {
    healthy_threshold   = "${var.hc_healthy_threshold}"
    unhealthy_threshold = "${var.hc_unhealthy_threshold}"
    timeout             = "${var.hc_timeout}"
    target              = "${var.hc_target}"
    interval            = "${var.hc_interval}"
  }

  tags {
    Name = "tf101-${var.server_type}-elb-YOURNAME-sg"
  }
}