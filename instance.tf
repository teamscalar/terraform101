/* First Terraform File

NOTE: Use ami-5648ad2c for Windows Server 2016 Base

*/

/* Setup access to AWS */
provider "aws" {
  region     = "us-east-1"
}

# Create an instance
resource "aws_instance" "instance_YOURNAME" {
  ami                         = "ami-8c1be5f6"
  instance_type               = "t2.micro"
  associate_public_ip_address = "true"
  vpc_security_group_ids      = ["${aws_security_group.web_YOURNAME.id}"]
  subnet_id                   = "subnet-49736b65"
  key_name                    = "us-east-key"

  tags {
    Name = "tf101_prod_instance_YOURNAME"
  }
}

# Create security group for instance
resource "aws_security_group" "web_YOURNAME" {
  name        = "tf101_prod_web_YOURNAME_sg"
  vpc_id      = "vpc-94bd76ec"

  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    ignore_changes = ["name"]
  }
  tags {
    Name = "tf101_prod_web_YOURNAME_sg"
  }
}