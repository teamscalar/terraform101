# terraform 101

This repo contains the Terraform files and a PDF of the presentation (MS-Terraform-Tutorial.pdf).

##NOTE: There is some strange formatting issue with the PDF so please DO NOT cut/paste the code.  It will break.

The preferred method is to actually type all of it out, but if you're completely stuck, you can
cut and paste from the Terraform files in this repository.  They are here for reference.

** Built and tested with Terraform v0.10.7.  Should work with later versions as well. **

## Prerequisites
* A Mac, Linux or Windows PC with Terraform installed and configured (see the PDF for instructions)
* A valid AWS API key (access key & secret key)
* Ideally a code editor of some sort (eg. Visual Studio Code or Sublime Text)

[Visual Studio Code](https://code.visualstudio.com/)

[Sublime Text](https://www.sublimetext.com/)
